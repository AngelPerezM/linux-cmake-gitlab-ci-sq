#include <iostream>

using namespace std;

int main(int argc, char* argv[]) {
  int num = argc - 1;

  if (num == 0) {
    cout << "No arguments provided\n";
  } else if (num == 2) {
    cout << "2 arguments provided\n";
  } else {
    cout << num << " arguments provided\n";
  }
  if (argv != 0) {
    cout << "argv not null\n";; // intentional extra-semicolon
  }
  if (argv == nullptr) {
    return 1;
  }

  return 0;
}

