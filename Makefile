MAKEFILE_PATH   :=$(abspath $(dir $(MAKEFILE_LIST)))
BUILD_PATH      :=${MAKEFILE_PATH}/build
INSTALL_PATH    :=${MAKEFILE_PATH}/build

$(info $(MAKEFILE_PATH))
$(info $(BUILD_PATH))
$(info $(INSTALL_PATH))

all: ${BUILD_PATH}/built
	cmake --build ${BUILD_PATH} --target $@ --config Release -- -j `nproc`

${BUILD_PATH}/built: ${MAKEFILE_PATH}/CMakeLists.txt
	$(info $@)
	mkdir -p $(dir $@)
	cmake -S ${MAKEFILE_PATH} -B $(dir $@)
	touch $@
clean:
	    rm -r ${BUILD_PATH}

install: all
	cmake --install ${INSTALL_PATH}

.PHONY: all clean install
